'use strict';

const fs = require('fs');
const path = require('path');


async function cleanWorkingDirFolder(foldername) {
    await fs.promises.rm(
        path.join(__dirname, '..', 'working_data', foldername),
        {
            recursive: true,
            force: true,
        }
    );
}


async function cleanWorkingDir() {
    await cleanWorkingDirFolder('configurations');
    await cleanWorkingDirFolder('content');
    await cleanWorkingDirFolder('logs');
}

async function copySeedsToWorkingDir() {

    const seed_data_path = path.join(__dirname, '..', 'seeds', 'data');
    const working_data_path = path.join(__dirname, '..', 'working_data');

    await fs.promises.cp(seed_data_path, working_data_path, {
        recursive: true,
    });
}


async function resetWorkingDir() {
    await cleanWorkingDir();
    await copySeedsToWorkingDir();
}

module.exports = {
    copySeedsToWorkingDir,
    cleanWorkingDir,
    resetWorkingDir,
};
