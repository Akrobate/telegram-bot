'use strict';

const path = require('path');
const {
    expect,
} = require('chai');
const {
    resetWorkingDir,
} = require('../test_helpers/DataLoader');
const {
    configuration,
} = require('../../src/configuration/configuration');
const {
    TasksAutoManagerService,
    ConfigurationsLoaderService,
} = require('../../src/services');

const configurations_loader_service = ConfigurationsLoaderService.getInstance();
const tasks_auto_manager_service = TasksAutoManagerService.getInstance();

describe('TasksAutoManagerService', () => {

    beforeEach(async () => {
        await resetWorkingDir();
    });

});
