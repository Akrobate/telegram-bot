'use strict';

const path = require('path');
const {
    expect,
} = require('chai');
const {
    resetWorkingDir,
} = require('../test_helpers/DataLoader');
const {
    configuration,
} = require('../../src/configuration/configuration');
const {
    ConfigurationsLoaderService,
} = require('../../src/services');

const configurations_loader_service = ConfigurationsLoaderService.getInstance();

describe('ConfigurationsLoaderService', () => {

    beforeEach(async () => {
        await resetWorkingDir();
    });

    it('loadAllTaksConfigurations', async () => {
        const result = await configurations_loader_service.loadAllTaksConfigurations(
            path.join(configuration.storage.folder, 'configurations/tasks/')
        );
        expect(result).to.have.property('task_1');
        expect(result).to.have.property('task_2');
        expect(result).to.have.property('task_3');
    });

    describe('getTaskKeyFromPath', () => {

        it('getTaskKeyFromPath - nominal case', () => {
            const result = configurations_loader_service
                .getTaskKeyFromPath('data/coucou/yellooow.json');
            expect(result).to.equal('yellooow');
        });

        it('getTaskKeyFromPath - no extension', () => {
            const result = configurations_loader_service
                .getTaskKeyFromPath('data/coucou/yellooow');
            expect(result).to.equal(null);
        });

        it('getTaskKeyFromPath - not json extension', () => {
            const result = configurations_loader_service
                .getTaskKeyFromPath('data/coucou/yellooow.txt');
            expect(result).to.equal(null);
        });

        it('getTaskKeyFromPath - filename without full path', () => {
            const result = configurations_loader_service
                .getTaskKeyFromPath('yellooow.json');
            expect(result).to.equal('yellooow');
        });
    });

});
