'use strict';

const path = require('path');
const {
    expect,
} = require('chai');
const {
    mock,
} = require('sinon');
const {
    resetWorkingDir,
} = require('../../test_helpers/DataLoader');
const {
    configuration,
} = require('../../../src/configuration/configuration');
const {
    OrderedFolderPublishTask,
} = require('../../../src/services/tasks');
const {
    TelegramRepository,
} = require('../../../src/repositories');
const {
    ConfigurationsLoaderService,
} = require('../../../src/services');

const configurations_loader_service = ConfigurationsLoaderService.getInstance();
const ordered_folder_publish_task = OrderedFolderPublishTask.getInstance();
ordered_folder_publish_task.setDataFolder(configuration.storage.folder);

const telegram_repository = TelegramRepository.getInstance();

describe('OrderedFolderPublishTask', () => {

    const mocks = {};

    beforeEach(async () => {
        mocks.telegram_repository = mock(telegram_repository);
        await resetWorkingDir();
    });

    afterEach(() => {
        mocks.telegram_repository.restore();
    });

    it('checkCanBeProcessed', async () => {
        const task_filename = 'task_3.json';
        const task_object = await configurations_loader_service
            .loadTaskConfiguration(
                path.join(
                    configuration.storage.folder,
                    'configurations/tasks/',
                    task_filename
                )
            );

        mocks.telegram_repository.expects('getMe').resolves({
            ok: true,
        });
        const result = await ordered_folder_publish_task.checkCanBeProcessed(task_object);
        expect(result).to.equal(true);
    });
});
