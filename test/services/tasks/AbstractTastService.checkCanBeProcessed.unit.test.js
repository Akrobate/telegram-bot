'use strict';

const path = require('path');
const {
    expect,
} = require('chai');
const {
    mock,
} = require('sinon');
const {
    resetWorkingDir,
} = require('../../test_helpers/DataLoader');
const {
    configuration,
} = require('../../../src/configuration/configuration');
const {
    RandomImagePublishTask,
} = require('../../../src/services/tasks');
const {
    TelegramRepository,
} = require('../../../src/repositories');
const {
    ConfigurationsLoaderService,
} = require('../../../src/services');


const configurations_loader_service = ConfigurationsLoaderService.getInstance();
const random_image_publish_task_service = RandomImagePublishTask.getInstance();
random_image_publish_task_service.setDataFolder(configuration.storage.folder);

const telegram_repository = TelegramRepository.getInstance();

describe('checkCanBeProcessed', () => {

    const mocks = {};
    let test_task_object = {};

    before(async () => {
        const task_filename = 'task_2.json';
        test_task_object = await configurations_loader_service
            .loadTaskConfiguration(
                path.join(
                    configuration.storage.folder,
                    'configurations/tasks/',
                    task_filename
                )
            );
    });

    beforeEach(async () => {
        mocks.telegram_repository = mock(telegram_repository);
        await resetWorkingDir();
    });

    afterEach(() => {
        mocks.telegram_repository.restore();
    });

    it('Nominal case', async () => {
        mocks.telegram_repository.expects('getMe').resolves({
            ok: true,
        });
        const result = await random_image_publish_task_service
            .checkCanBeProcessed(test_task_object);
        expect(result).to.equal(true);
    });

    it('no chat_id', async () => {
        const task_object = {
            ...test_task_object,
        };

        task_object.chat_id = null;
        let result = await random_image_publish_task_service.checkCanBeProcessed(task_object);
        expect(result).to.equal(false);

        task_object.chat_id = undefined;
        result = await random_image_publish_task_service.checkCanBeProcessed(task_object);
        expect(result).to.equal(false);

        task_object.chat_id = '';
        result = await random_image_publish_task_service.checkCanBeProcessed(task_object);
        expect(result).to.equal(false);

        task_object.chat_id = 0;
        result = await random_image_publish_task_service.checkCanBeProcessed(task_object);
        expect(result).to.equal(false);
    });

    it('no type', async () => {
        const task_object = {
            ...test_task_object,
        };

        task_object.type = null;
        let result = await random_image_publish_task_service.checkCanBeProcessed(task_object);
        expect(result).to.equal(false);

        task_object.type = undefined;
        result = await random_image_publish_task_service.checkCanBeProcessed(task_object);
        expect(result).to.equal(false);

        task_object.type = '';
        result = await random_image_publish_task_service.checkCanBeProcessed(task_object);
        expect(result).to.equal(false);

        task_object.type = 0;
        result = await random_image_publish_task_service.checkCanBeProcessed(task_object);
        expect(result).to.equal(false);
    });


    it('no to_publish_folder', async () => {
        const task_object = {
            ...test_task_object,
        };

        task_object.to_publish_folder = null;
        let result = await random_image_publish_task_service.checkCanBeProcessed(task_object);
        expect(result).to.equal(false);

        task_object.to_publish_folder = undefined;
        result = await random_image_publish_task_service.checkCanBeProcessed(task_object);
        expect(result).to.equal(false);

        task_object.to_publish_folder = '';
        result = await random_image_publish_task_service.checkCanBeProcessed(task_object);
        expect(result).to.equal(false);

        task_object.to_publish_folder = 0;
        result = await random_image_publish_task_service.checkCanBeProcessed(task_object);
        expect(result).to.equal(false);
    });


    it('no published_folder', async () => {
        const task_object = {
            ...test_task_object,
        };

        task_object.published_folder = null;
        let result = await random_image_publish_task_service.checkCanBeProcessed(task_object);
        expect(result).to.equal(false);

        task_object.published_folder = undefined;
        result = await random_image_publish_task_service.checkCanBeProcessed(task_object);
        expect(result).to.equal(false);

        task_object.published_folder = '';
        result = await random_image_publish_task_service.checkCanBeProcessed(task_object);
        expect(result).to.equal(false);

        task_object.published_folder = 0;
        result = await random_image_publish_task_service.checkCanBeProcessed(task_object);
        expect(result).to.equal(false);
    });


    it('getMe not responding ok', async () => {
        const task_object = {
            ...test_task_object,
        };
        mocks.telegram_repository.expects('getMe').resolves({
            ok: false,
        });
        const result = await random_image_publish_task_service.checkCanBeProcessed(task_object);
        expect(result).to.equal(false);
    });


    it('getMe not throwing error', async () => {
        const task_object = {
            ...test_task_object,
        };
        mocks.telegram_repository.expects('getMe').rejects(new Error());
        const result = await random_image_publish_task_service.checkCanBeProcessed(task_object);
        expect(result).to.equal(false);
    });
});
