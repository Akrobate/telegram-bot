'use strict';

const {
    CustomSortService,
} = require('../../../src/services/commons');
const {
    expect,
} = require('chai');

const custom_sort_service = CustomSortService.getInstance();

const list_1 = [
    'post_1.removed',
    'post_10',
    'post_11',
    'post_12',
    'post_13.removed',
    'post_14',
    'post_15',
    'post_16',
    'post_17',
    'post_18',
    'post_19.collab',
    'post_2',
    'post_20',
    'post_21',
    'post_22',
    'post_23',
    'post_24',
    'post_25.removed',
    'post_26',
    'post_27',
    'post_28',
    'post_29',
    'post_3',
    'post_30',
    'post_31',
    'post_32.removed',
    'post_33',
    'post_34',
    'post_35',
    'post_36',
    'post_37',
    'post_4',
    'post_5.removed',
    'post_6',
    'post_7',
    'post_8.removed',
    'post_9',
    'ready',
];


describe('Sorting custom function', () => {

    it('humanAlphaNumericalSort', () => {
        const result = custom_sort_service.humanAlphaNumericalSort(list_1);
        const [
            element_1,
            element_2,
            element_3,
        ] = result;
        expect([
            element_1,
            element_2,
            element_3,
        ]).to.deep.equal([
            'ready',
            'post_1.removed',
            'post_2',
        ]);
    });

    it('parseToSortableValue', () => {
        const result = custom_sort_service
            .parseToSortableValue('post_554._4111removed');
        expect(result).to.equal(554);
    });

    it('parseToSortableValue - no numerical chars', () => {
        const result = custom_sort_service
            .parseToSortableValue('no_numerical_chars');
        expect(result).to.equal(0);
    });

    it('humanAlphaNumericalSort', () => {

        const seed = [
            'wtfd',
            'post 4 coucou 6 and 7',
            'post_4.6',
            'post_2',
            'post_1 - 2 there',
            'post_3',
            'post_1 - 1_here',
            'post_4 - 5 toto',
            'post_1',
        ];

        const result = custom_sort_service
            .humanAlphaNumericalSort(seed);
        expect(result).to.deep.equal([
            'wtfd',
            'post_1',
            'post_1 - 1_here',
            'post_1 - 2 there',
            'post_2',
            'post_3',
            'post_4 - 5 toto',
            'post_4.6',
            'post 4 coucou 6 and 7',
        ]);
    });


    it('humanAlphaNumericalSort - sames values sort', () => {
        const seed = [
            'wtfd',
            'wtfd',
        ];
        const result = custom_sort_service
            .humanAlphaNumericalSort(seed);
        expect(result).to.deep.equal([
            'wtfd',
            'wtfd',
        ]);
    });


    it('parseToSortableZonesValue', () => {
        const result = custom_sort_service
            .parseToSortableZonesValue('post_554._4111removed');
        expect(result).to.deep.equal(['post_554._', '4111removed']);
    });


    it('getSortableValuesList', () => {
        const result = custom_sort_service
            .getSortableValuesList('post_554._4111removed');
        expect(result).to.deep.equal([554, 4111]);
    });


    it('getSortableValuesList - one value', () => {
        const result = custom_sort_service
            .getSortableValuesList('post_1');
        expect(result).to.deep.equal([1]);
    });


    it('getSortableValuesList - no values', () => {
        const result = custom_sort_service
            .getSortableValuesList('post_of something great but no value');
        expect(result).to.deep.equal([]);
    });
});
