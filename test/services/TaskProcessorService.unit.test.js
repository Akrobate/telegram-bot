'use strict';

const path = require('path');
const {
    expect,
} = require('chai');
const {
    mock,
} = require('sinon');
const {
    resetWorkingDir,
} = require('../test_helpers/DataLoader');
const {
    configuration,
} = require('../../src/configuration/configuration');
const {
    TaskProcessorService,
    ConfigurationsLoaderService,
} = require('../../src/services');
const {
    TelegramRepository,
} = require('../../src/repositories');


const configurations_loader_service = ConfigurationsLoaderService.getInstance();
const tasks_processor_service = TaskProcessorService.getInstance();
const telegram_repository = TelegramRepository.getInstance();

describe('TaskProcessorService', () => {

    const mocks = {};

    beforeEach(async () => {
        await resetWorkingDir();
        mocks.telegram_repository = mock(telegram_repository);
    });

    afterEach(() => {
        mocks.telegram_repository.restore();
    });


    describe('RandomImagePublishTask', () => {

        it('check', async () => {
            const task_filename = 'task_1.json';
            const task_object = await configurations_loader_service
                .loadTaskConfiguration(
                    path.join(
                        configuration.storage.folder,
                        'configurations/tasks/',
                        task_filename
                    )
                );

            mocks.telegram_repository.expects('getMe')
                .resolves({
                    ok: true,
                });

            const result = await tasks_processor_service
                .check(task_object);

            expect(result).to.equal(true);
        });


        it('process', async () => {
            const task_filename = 'task_1.json';
            const task_object = await configurations_loader_service
                .loadTaskConfiguration(
                    path.join(
                        configuration.storage.folder,
                        'configurations/tasks/',
                        task_filename
                    )
                );

            mocks.telegram_repository.expects('getMe')
                .resolves({
                    ok: true,
                });

            mocks.telegram_repository.expects('sendPhoto')
                .callsFake(
                    (api_key, chat_id, file_path, caption, parse_mode) => {
                        expect(api_key).to.equal(task_object.bot_api_key);
                        expect(chat_id).to.equal(task_object.chat_id);
                        expect(parse_mode).to.equal(undefined);
                        expect(task_object.params.captions).to.include(caption);
                        return Promise.resolve();
                    }
                );

            await tasks_processor_service
                .process(task_object);

        });
    });

    describe('OrderedFolderPublishTask', () => {
        it('check', async () => {
            const task_filename = 'task_3.json';
            const task_object = await configurations_loader_service
                .loadTaskConfiguration(
                    path.join(
                        configuration.storage.folder,
                        'configurations/tasks/',
                        task_filename
                    )
                );

            mocks.telegram_repository.expects('getMe')
                .resolves({
                    ok: true,
                });

            const result = await tasks_processor_service
                .check(task_object);

            expect(result).to.equal(true);
        });


        it('process', async () => {
            const task_filename = 'task_3.json';
            const task_object = await configurations_loader_service
                .loadTaskConfiguration(
                    path.join(
                        configuration.storage.folder,
                        'configurations/tasks/',
                        task_filename
                    )
                );

            mocks.telegram_repository.expects('getMe')
                .resolves({
                    ok: true,
                });

            mocks.telegram_repository.expects('sendPhoto')
                .callsFake(
                    // eslint-disable-next-line no-unused-vars
                    (api_key, chat_id, file_path, caption, parse_mode) => {
                        expect(api_key).to.equal(task_object.bot_api_key);
                        expect(chat_id).to.equal(task_object.chat_id);
                        return Promise.resolve();
                    }
                );

            await tasks_processor_service
                .process(task_object);

        });

        it('process unexisting caption file', async () => {
            const task_filename = 'task_3.json';
            const original_task_object = await configurations_loader_service
                .loadTaskConfiguration(
                    path.join(
                        configuration.storage.folder,
                        'configurations/tasks/',
                        task_filename
                    )
                );

            const task_object = {
                ...original_task_object,
                params: {
                    image_filename_to_publish: original_task_object.params
                        .image_filename_to_publish,
                    text_filename_to_publish: 'NOT_EXISTING_FILE.txt',
                },
            };

            mocks.telegram_repository.expects('getMe').twice()
                .resolves({
                    ok: true,
                });

            await tasks_processor_service
                .process(task_object);

            await tasks_processor_service
                .process(task_object);

            mocks.telegram_repository.verify();
        });


    });
});
