'use strict';

const {
    TelegramRepository,
} = require('../../src/repositories/');
const {
    expect,
} = require('chai');
const {
    mock,
} = require('sinon');
const axios = require('axios');

describe('TelegramRepository', () => {

    const telegram_repository = TelegramRepository.getInstance();
    const mocks = {};
    beforeEach(() => {
        mocks.axios = mock(axios);
    });

    afterEach(() => {
        mocks.axios.restore();
    });

    it('sendMessage', async () => {

        const api_key = 'API_KEY';
        const chat_id = 'CHAT_ID';
        const text = 'Lorem ipsum sic amet dolorit';

        mocks.axios.expects('get')
            .once()
            .withArgs(
                `https://api.telegram.org/bot${api_key}/sendMessage`,
                {
                    params: {
                        chat_id,
                        text,
                    },
                }
            )
            .resolves({
                data: {
                    ok: true,
                },
            });
        const response = await telegram_repository.sendMessage(api_key, chat_id, text);
        expect(response).to.have.property('ok', true);
        mocks.axios.verify();
    });


    it('deleteMessage', async () => {

        const api_key = 'API_KEY';
        const chat_id = 'CHAT_ID';
        const message_id = 'MESSAGE_ID';

        mocks.axios.expects('get')
            .once()
            .withArgs(
                `https://api.telegram.org/bot${api_key}/deleteMessage`,
                {
                    params: {
                        chat_id,
                        message_id,
                    },
                }
            )
            .resolves({
                data: {
                    ok: true,
                },
            });
        const response = await telegram_repository.deleteMessage(api_key, chat_id, message_id);
        expect(response).to.have.property('ok', true);
        mocks.axios.verify();
    });

});
