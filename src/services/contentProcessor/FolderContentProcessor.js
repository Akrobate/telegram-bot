'use strict';

const path = require('path');
class FolderContentProcessor {


    /**
     * @static
     * @returns {FolderContentProcessor}
     */
    static getInstance() {
        if (FolderContentProcessor.instance === null) {
            FolderContentProcessor.instance = new FolderContentProcessor();
        }
        return FolderContentProcessor.instance;
    }


    /**
     *
     * @param {*} params
     * @param {*} first_folder
     * @param {*} to_publish_folder
     * @param {string} id
     * @returns {Object}
     */
    async process(params, first_folder, to_publish_folder, id) {

        let image_filename_to_publish = null;
        let text_filename_to_publish = null;
        if (params !== undefined) {
            // eslint-disable-next-line prefer-destructuring
            image_filename_to_publish = params.image_filename_to_publish;
            // eslint-disable-next-line prefer-destructuring
            text_filename_to_publish = params.text_filename_to_publish;
        }

        let selected_file = null;
        if (
            image_filename_to_publish === null
            || image_filename_to_publish === undefined
        ) {
            const image_file_list = await this.file_repository
                .listFilesInDirectoryWithExtention(
                    path.join(this.data_folder, to_publish_folder, first_folder),
                    ['png', 'jpg', 'jpeg']
                );

            [selected_file] = image_file_list;
        } else {
            selected_file = image_filename_to_publish;
        }


        let _text_filename_to_publish = null;
        if (
            text_filename_to_publish === null
            || text_filename_to_publish === undefined
        ) {
            const txt_file_list = await this.file_repository
                .listFilesInDirectoryWithExtention(
                    path.join(this.data_folder, to_publish_folder, first_folder),
                    ['txt', 'md']
                );
            const [
                text_file_name,
            ] = txt_file_list;
            _text_filename_to_publish = text_file_name;
        } else {
            _text_filename_to_publish = text_filename_to_publish;
        }

        const caption_full_filename = path.join(
            this.data_folder, to_publish_folder, first_folder, _text_filename_to_publish
        );

        if (!this.file_repository.exists(caption_full_filename)) {
            // return;
            throw Error(`id: ${id} - ERROR ${first_folder} - caption ${_text_filename_to_publish} not exists`);
        }

        const caption = await this.file_repository.readFileUtf8(caption_full_filename);

        const parse_mode = this.file_repository
            .hasExtention(_text_filename_to_publish, 'md')
            ? 'MarkdownV2'
            : undefined;

        return {
            selected_file,
            caption,
            parse_mode,
        };
    }

}

FolderContentProcessor.instance = null;

module.exports = {
    FolderContentProcessor,
};
