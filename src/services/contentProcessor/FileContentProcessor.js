'use strict';

class FileContentProcessor {


    /**
     * @static
     * @returns {FileContentProcessor}
     */
    static getInstance() {
        if (FileContentProcessor.instance === null) {
            FileContentProcessor.instance = new FileContentProcessor();
        }
        return FileContentProcessor.instance;
    }


    /**
     * @param {*} params
     * @returns {Object}
     */
    process(params) {
        const {
            captions,
        } = params;
        let caption = null;
        if (captions !== undefined) {
            caption = this.selectRandonCaptionFromList(captions);
        }
        return {
            caption,
        };
    }


    /**
     * @param {Array<String>} caption_list
     * @returns {string}
     */
    selectRandonCaptionFromList(caption_list) {
        const num = Math.floor(Math.random() * caption_list.length);
        return caption_list[num];
    }
}

FileContentProcessor.instance = null;

module.exports = {
    FileContentProcessor,
};
