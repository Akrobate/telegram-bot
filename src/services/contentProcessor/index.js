'use strict';

const {
    FileContentProcessor,
} = require('./FileContentProcessor');

const {
    FolderContentProcessor,
} = require('./FolderContentProcessor');

module.exports = {
    FileContentProcessor,
    FolderContentProcessor,
};
