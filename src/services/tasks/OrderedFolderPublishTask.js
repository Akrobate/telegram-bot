'use strict';

const path = require('path');

const {
    AbstractTaskService,
} = require('./AbstractTaskService');

const {
    JsonFileRepository,
    FileRepository,
    TelegramRepository,
} = require('../../repositories');

const {
    CustomSortService,
} = require('../commons/CustomSortService');

/**
 * may be overloaded here
 * checkCanBeProcessed(task)
 */
class OrderedFolderPublishTask extends AbstractTaskService {


    /**
     * @static
     * @returns {OrderedFolderPublishTask}
     */
    static getInstance() {
        if (OrderedFolderPublishTask.instance === null) {
            OrderedFolderPublishTask.instance = new OrderedFolderPublishTask(
                JsonFileRepository.getInstance(),
                FileRepository.getInstance(),
                TelegramRepository.getInstance(),
                CustomSortService.getInstance()
            );
        }
        return OrderedFolderPublishTask.instance;
    }


    /**
     * @param {JsonFileRepository} json_file_repository
     * @param {FileRepository} file_repository
     * @param {TelegramRepository} telegram_repository
     * @param {CustomSortService} custom_sort_service
     */
    constructor(
        json_file_repository,
        file_repository,
        telegram_repository,
        custom_sort_service
    ) {
        super(
            json_file_repository,
            file_repository,
            telegram_repository
        );
        this.json_file_repository = json_file_repository;
        this.file_repository = file_repository;
        this.telegram_repository = telegram_repository;
        this.custom_sort_service = custom_sort_service;
    }

    /**
     * @static
     */
    static get type() {
        return 'ORDERED_FOLDER_PUBLISH';
    }


    /**
     * @param {Object} task
     * @return {void}
     */
    async process(task) {

        const task_can_be_processed = await this.checkCanBeProcessed(task);
        if (!task_can_be_processed) {
            return;
        }

        const {
            id,
            to_publish_folder,
            published_folder,
            log_file,
            bot_api_key,
            chat_id,
            params,
        } = task;


        let image_filename_to_publish = null;
        let text_filename_to_publish = null;
        if (params !== undefined) {
            // eslint-disable-next-line prefer-destructuring
            image_filename_to_publish = params.image_filename_to_publish;
            // eslint-disable-next-line prefer-destructuring
            text_filename_to_publish = params.text_filename_to_publish;
        }

        let selected_folder = null;
        let to_publish_count = null;

        try {
            ({
                selected_folder,
                to_publish_count,
            } = await this.selectFolder(id, to_publish_folder, log_file));
        } catch (_error) {
            this.logToFile(
                path.join(this.data_folder, log_file),
                `id: ${id} - Error Nothing to publish`
            );
            return;
        }

        let selected_file = null;
        if (
            image_filename_to_publish === null
            || image_filename_to_publish === undefined
        ) {
            const image_file_list = await this.file_repository
                .listFilesInDirectoryWithExtention(
                    path.join(this.data_folder, to_publish_folder, selected_folder),
                    ['png', 'jpg', 'jpeg']
                );

            [selected_file] = image_file_list;
        } else {
            selected_file = image_filename_to_publish;
        }

        let caption = '';
        let parse_mode = null;
        try {
            ({
                caption,
                parse_mode,
            } = await this.getCaptionFromFolder(
                id,
                text_filename_to_publish,
                to_publish_folder,
                selected_folder,
                log_file
            ));
        } catch (_) {
            return;
        }

        await this.telegram_repository.sendPhoto(
            bot_api_key,
            chat_id,
            path.join(this.data_folder, to_publish_folder, selected_folder, selected_file),
            caption,
            parse_mode
        );

        await this.file_repository.moveFile(
            path.join(this.data_folder, to_publish_folder, selected_folder),
            path.join(this.data_folder, published_folder, selected_folder)
        );

        const formated_caption_for_log = this.formatCaptionForLog(caption);
        const log_string = `id: ${id} - Published ${selected_file} - ${formated_caption_for_log} - to publish: ${to_publish_count}`;

        await this.logToFile(
            path.join(this.data_folder, log_file),
            log_string
        );
    }


    /**
     * @param {id} id
     * @param {*} to_publish_folder
     * @param {*} log_file
     * @return {*}
     */
    async selectFolder(id, to_publish_folder) {
        const to_publish_file_list = await this.file_repository
            .listDirectoriesInDirectory(path.join(this.data_folder, to_publish_folder));

        const to_publish_count = to_publish_file_list.length;

        if (to_publish_count === 0) {
            throw new Error('to_publish_count = 0');
        }

        const ordered_to_publish_file_list = this.custom_sort_service
            .humanAlphaNumericalSort(to_publish_file_list);

        const [
            selected_folder,
        ] = ordered_to_publish_file_list;

        // eslint-disable-next-line consistent-return
        return {
            selected_folder,
            to_publish_count,
        };
    }


    /**
     * @param {*} id
     * @param {*} text_filename_to_publish
     * @param {*} to_publish_folder
     * @param {*} selected_folder
     * @param {*} log_file
     * @returns {Object}
     */
    async getCaptionFromFolder(
        id,
        text_filename_to_publish,
        to_publish_folder,
        selected_folder,
        log_file
    ) {

        let _text_filename_to_publish = null;
        if (
            text_filename_to_publish === null
            || text_filename_to_publish === undefined
        ) {
            const txt_file_list = await this.file_repository
                .listFilesInDirectoryWithExtention(
                    path.join(this.data_folder, to_publish_folder, selected_folder),
                    ['txt', 'md']
                );
            const [
                text_file_name,
            ] = txt_file_list;
            _text_filename_to_publish = text_file_name;
        } else {
            _text_filename_to_publish = text_filename_to_publish;
        }

        const caption_full_filename = path.join(
            this.data_folder, to_publish_folder, selected_folder, _text_filename_to_publish
        );

        if (!this.file_repository.exists(caption_full_filename)) {
            await this.logToFile(
                path.join(this.data_folder, log_file),
                `id: ${id} - ERROR ${selected_folder} - caption ${_text_filename_to_publish} not exists`
            );
            throw new Error('ERROR caption not exists');
        }

        const caption = await this.file_repository.readFileUtf8(caption_full_filename);

        const parse_mode = this.file_repository
            .hasExtention(_text_filename_to_publish, 'md')
            ? 'MarkdownV2'
            : undefined;

        // eslint-disable-next-line consistent-return
        return {
            caption,
            parse_mode,
        };

    }

}

OrderedFolderPublishTask.instance = null;

module.exports = {
    OrderedFolderPublishTask,
};
