const {
    OrderedFolderPublishTask,
} = require('./OrderedFolderPublishTask');

const {
    RandomImagePublishTask,
} = require('./RandomImagePublishTask');

module.exports = {
    RandomImagePublishTask,
    OrderedFolderPublishTask,
};
