'use strict';

const path = require('path');

const {
    AbstractTaskService,
} = require('./AbstractTaskService');

const {
    JsonFileRepository,
    FileRepository,
    TelegramRepository,
} = require('../../repositories');


/**
 * may be overloaded here
 * checkCanBeProcessed(task)
 */

class RandomImagePublishTask extends AbstractTaskService {


    /**
     * @static
     * @returns {RandomImagePublishTask}
     */
    static getInstance() {
        if (RandomImagePublishTask.instance === null) {
            RandomImagePublishTask.instance = new RandomImagePublishTask(
                JsonFileRepository.getInstance(),
                FileRepository.getInstance(),
                TelegramRepository.getInstance()
            );
        }
        return RandomImagePublishTask.instance;
    }


    /**
     * @static
     */
    static get type() {
        return 'RANDOM_IMAGE_PUBLISH';
    }


    /**
     * @param {Object} task
     * @return {void}
     */
    async process(task) {

        const task_can_be_processed = await this.checkCanBeProcessed(task);
        if (!task_can_be_processed) {
            return;
        }

        const {
            id,
            to_publish_folder,
            published_folder,
            log_file,
            bot_api_key,
            chat_id,
            params,
        } = task;

        const {
            captions,
        } = params;

        const to_publish_file_list = await this.file_repository
            .listFilesInDirectory(path.join(this.data_folder, to_publish_folder));

        const to_publish_count = to_publish_file_list.length;

        if (to_publish_count === 0) {
            this.logToFile(
                path.join(this.data_folder, log_file),
                `id: ${id} - Error Nothing to publish`
            );
            return;
        }

        const selected_file = this
            .selectRandomFileFromList(to_publish_file_list);

        let caption = null;
        if (captions !== undefined) {
            caption = this.selectRandonCaptionFromList(captions);
        }

        // Publish
        await this.telegram_repository.sendPhoto(
            bot_api_key,
            chat_id,
            path.join(this.data_folder, to_publish_folder, selected_file),
            caption
        );

        await this.file_repository.moveFile(
            path.join(this.data_folder, to_publish_folder, selected_file),
            path.join(this.data_folder, published_folder, selected_file)
        );

        const formated_caption_for_log = this.formatCaptionForLog(caption);
        const log_string = `id: ${id} - Published ${selected_file} - ${formated_caption_for_log} - to publish: ${to_publish_count}`;

        await this.logToFile(
            path.join(this.data_folder, log_file),
            log_string
        );
    }


    /**
     * @param {Array<string>} file_list
     * @returns {string}
     */
    selectRandomFileFromList(file_list) {
        const num = Math.floor(Math.random() * file_list.length);
        return file_list[num];
    }


    /**
     * @param {Array<String>} caption_list
     * @returns {string}
     */
    selectRandonCaptionFromList(caption_list) {
        const num = Math.floor(Math.random() * caption_list.length);
        return caption_list[num];
    }
}

RandomImagePublishTask.instance = null;

module.exports = {
    RandomImagePublishTask,
};
