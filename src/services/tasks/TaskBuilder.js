'use strict';

class Taskbuilder {


    /**
     * @static
     * @returns {Taskbuilder}
     */
    static getInstance() {
        if (Taskbuilder.instance === null) {
            Taskbuilder.instance = new Taskbuilder();
        }
        return Taskbuilder.instance;
    }

}

Taskbuilder.instance = null;

module.exports = {
    Taskbuilder,
};
