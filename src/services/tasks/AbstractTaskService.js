'use strict';

const path = require('path');
const {
    DateTime,
} = require('luxon');

const {
    configuration,
} = require('../../configuration/configuration');

class AbstractTaskService {

    /**
     * @param {JsonFileRepository} json_file_repository
     * @param {FileRepository} file_repository
     * @param {TelegramRepository} telegram_repository
     */
    constructor(
        json_file_repository,
        file_repository,
        telegram_repository
    ) {
        this.json_file_repository = json_file_repository;
        this.file_repository = file_repository;
        this.telegram_repository = telegram_repository;

        this.data_folder = configuration.storage.folder;
    }


    /**
     * @returns {Void}
     */
    async process() {

        // select files / or folder
        const content = await this.publishable_content.get(); // publishable_content <= to create / to decide layer name

        // select entity
        const selected_content = await this.content_selector.process(content);

        // process entity
        const to_publish = await this.content_processor.process(selected_content);

        // publish
        await this.publisher.publish(to_publish);
    }


    /**
     * @param {*} data_folder
     * @returns {Void}
     */
    setDataFolder(data_folder) {
        this.data_folder = data_folder;
    }


    /**
     * @param {Object} task
     * @return {Boolean}
     */
    async checkCanBeProcessed(task) {
        const {
            id,
            to_publish_folder,
            log_file,
            published_folder,
            chat_id,
            type,
            bot_api_key,
        } = task;

        if (
            chat_id === null || chat_id === undefined || chat_id === 0 || chat_id === ''
        ) {
            this.logToFile(
                path.join(this.data_folder, log_file),
                `id: ${id} - Error: chat_id is not defined`
            );
            return false;
        }


        if (
            type === null || type === undefined || type === 0 || type === ''
        ) {
            this.logToFile(
                path.join(this.data_folder, log_file),
                `id: ${id} - Error: type is not defined`
            );
            return false;
        }

        if (
            !to_publish_folder
            || !this.file_repository.exists(path.join(this.data_folder, to_publish_folder))
        ) {
            this.logToFile(
                path.join(this.data_folder, log_file),
                `id: ${id} - Error: to_publish_folder not exists`
            );
            return false;
        }

        if (
            !published_folder
            || !this.file_repository.exists(path.join(this.data_folder, published_folder))
        ) {
            this.logToFile(
                path.join(this.data_folder, log_file),
                `id: ${id} - Error: published_folder not exists`
            );
            return false;
        }

        let api_result = {};
        try {
            api_result = await this.telegram_repository.getMe(bot_api_key);
            if (!api_result.ok) {
                this.logToFile(
                    path.join(this.data_folder, log_file),
                    `id: ${id} - Error: bot_api_key Unauthorized`
                );
                return false;
            }
        } catch (error) {
            this.logToFile(
                path.join(this.data_folder, log_file),
                `id: ${id} - Error: bot_api_key Unauthorized`
            );
            return false;
        }

        return true;
    }


    /**
     * @param {string} file
     * @param {string} content
     * @return {void}
     */
    async logToFile(file, content) {
        const date = DateTime.now().toFormat('hh:mm:ss dd/MM/yy');
        await this.file_repository
            .appendFile(file, `${date} - ${content}\n`);
        console.log(`${date} - ${content}`);
    }


    /**
     * @param {String} caption
     * @returns {String}
     */
    formatCaptionForLog(caption) {
        return caption
            ? `${caption
                .replaceAll('\n', ' ').replaceAll('  ', ' ')
                .slice(0, 15)}[...]`
            : 'None';
    }
}

module.exports = {
    AbstractTaskService,
};
