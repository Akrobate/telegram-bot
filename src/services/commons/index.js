'use strict';

const {
    CommandLineParamsService,
} = require('./CommandLineParamsService');

const {
    CustomSortService,
} = require('./CustomSortService');

module.exports = {
    CommandLineParamsService,
    CustomSortService,
};
