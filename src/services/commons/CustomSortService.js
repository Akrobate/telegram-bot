'use strict';

class CustomSortService {

    /* istanbul ignore next */
    /**
     * @static
     * @returns {CustomSortService}
     */
    static getInstance() {
        if (CustomSortService.instance === null) {
            CustomSortService.instance = new CustomSortService();
        }
        return CustomSortService.instance;
    }


    /**
     * @param {String} input
     * @return {Number}
     */
    parseToSortableValue(input) {
        const match_result = input.match(/(\d+)/);
        if (match_result) {
            const [
                ,
                result,
            ] = input.match(/(\d+)/);
            return result ? Number(result) : 0;
        }
        return 0;
    }


    /**
     * @param {String} input
     * @return {array}
     */
    parseToSortableZonesValue(input) {
        const match_result = input.match(/\D*(\d+)\D*/g);
        if (match_result) {
            const zones = input.match(/\D*(\d+)\D*/g);
            return zones;
        }
        return [];
    }

    /**
     * @param {String} input
     * @return {array}
     */
    getSortableValuesList(input) {
        return this.parseToSortableZonesValue(input).map(
            (item) => this.parseToSortableValue(item)
        );
    }

    /**
     * @param {Array<string>} string_list
     * @param {Boolean} asc
     * @returns {Array<string>}
     */
    humanAlphaNumericalSort(string_list, asc = true) {
        const sorted = [
            ...string_list,
        ];

        sorted.sort((a, b) => {
            const a_list = this.getSortableValuesList(a);
            const b_list = this.getSortableValuesList(b);

            const max = Math.max(a_list.length, b_list.length);
            for (let i = 0; i < max; i++) {
                if (a_list[i] === undefined) {
                    return asc ? -1 : 1;
                }
                if (b_list[i] === undefined) {
                    return asc ? 1 : -1;
                }

                if (a_list[i] > b_list[i]) {
                    return asc ? 1 : -1;
                }
                if (a_list[i] < b_list[i]) {
                    return asc ? -1 : 1;
                }

            }
            return 0;
        });

        return sorted;
    }

}

CustomSortService.instance = null;

module.exports = {
    CustomSortService,
};
