'use strict';

const logger = require('../logger');
const {
    RandomImagePublishTask,
    OrderedFolderPublishTask,
} = require('./tasks');

class TaskProcessorService {

    /**
     * @static
     * @returns {TaskProcessorService}
     */
    static getInstance() {
        if (TaskProcessorService.instance === null) {
            TaskProcessorService.instance = new TaskProcessorService(
                RandomImagePublishTask.getInstance(),
                OrderedFolderPublishTask.getInstance()
            );
        }
        return TaskProcessorService.instance;
    }


    /**
     * @param {RandomImagePublishTask} random_image_publish_task_service
     * @param {OrderedFolderPublishTask} ordered_folder_publish_task_service
     */
    constructor(
        random_image_publish_task_service,
        ordered_folder_publish_task_service
    ) {
        this.random_image_publish_task_service = random_image_publish_task_service;
        this.ordered_folder_publish_task_service = ordered_folder_publish_task_service;
    }


    /**
     * @param {object} task
     * @returns {Promise}
     */
    process(task) {
        return this
            .taskServiceSelector(task)
            .process(task);
    }


    /**
     * @param {object} task
     * @returns {Promise}
     */
    check(task) {
        return this
            .taskServiceSelector(task)
            .checkCanBeProcessed(task);
    }


    /**
     * @param {Object} task
     * @returns {Promise<Object>}
     */
    taskServiceSelector(task) {
        const {
            type,
        } = task;

        let service = null;
        switch (type) {
            case RandomImagePublishTask.type:
                service = this.random_image_publish_task_service;
                break;
            case OrderedFolderPublishTask.type:
                service = this.ordered_folder_publish_task_service;
                break;
            default:
                logger.log(`Unknown task type: ${type}`);
        }
        return service;
    }
}

TaskProcessorService.instance = null;

module.exports = {
    TaskProcessorService,
};
