'use strict';

const path = require('path');

const {
    JsonFileRepository,
    FileRepository,
} = require('../repositories');


class ConfigurationsLoaderService {

    /* istanbul ignore next */
    /**
     * @static
     * @returns {ConfigurationsLoaderService}
     */
    static getInstance() {
        if (ConfigurationsLoaderService.instance === null) {
            ConfigurationsLoaderService.instance = new ConfigurationsLoaderService(
                JsonFileRepository.getInstance(),
                FileRepository.getInstance()
            );
        }
        return ConfigurationsLoaderService.instance;
    }


    /**
     * @param {JsonFileRepository} json_file_repository
     * @param {FileRepository} file_repository
     */
    constructor(
        json_file_repository,
        file_repository
    ) {
        this.json_file_repository = json_file_repository;
        this.file_repository = file_repository;
    }


    /**
     * @param {string} json_files_folder
     * @return {Object}
     */
    async loadAllTaksConfigurations(json_files_folder) {
        const files = await this.file_repository
            .listFilesInDirectoryWithExtention(json_files_folder, ['json']);

        const result = {};
        for (const file of files) {
            const configuration_object_key = file.replace('.json', '');
            result[configuration_object_key] = await this
                .loadTaskConfiguration(path.join(json_files_folder, file));
        }
        return result;
    }


    /**
     * @param {string} task_configuration_file
     * @return {Object}
     */
    async loadTaskConfiguration(task_configuration_file) {
        const conf = await this
            .json_file_repository.getData(task_configuration_file);
        return {
            ...conf,
            task_configuration_file: path
                .basename(task_configuration_file),
        };
    }


    /**
     * @param {String} path_filename
     * @returns {String}
     */
    getTaskKeyFromPath(path_filename) {
        const filename = path.basename(path_filename);
        const exploded_file_name = filename.split('.');
        if (exploded_file_name.length === 1) {
            return null;
        }
        if (exploded_file_name[exploded_file_name.length - 1] !== 'json') {
            return null;
        }
        return filename.replace('.json', '');
    }
}

ConfigurationsLoaderService.instance = null;

module.exports = {
    ConfigurationsLoaderService,
};
