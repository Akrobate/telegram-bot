'use strict';

const {
    OrderedSelector,
} = require('./OrderedSelector');

const {
    RandomSelector,
} = require('./RandomSelector');

module.exports = {
    OrderedSelector,
    RandomSelector,
};
