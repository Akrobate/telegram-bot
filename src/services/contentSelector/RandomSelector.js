'use strict';

class RandomSelector {


    /**
     * @static
     * @returns {RandomSelector}
     */
    static getInstance() {
        if (RandomSelector.instance === null) {
            RandomSelector.instance = new RandomSelector();
        }
        return RandomSelector.instance;
    }


    /**
     * @param {Array} list
     * @return {String}
     */
    process(list) {
        const num = Math.floor(Math.random() * list.length);
        return list[num];
    }
}

RandomSelector.instance = null;

module.exports = {
    RandomSelector,
};
