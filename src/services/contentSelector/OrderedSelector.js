'use strict';

const {
    CustomSortService,
} = require('../commons');

class OrderedSelector {


    /**
     * @static
     * @returns {OrderedSelector}
     */
    static getInstance() {
        if (OrderedSelector.instance === null) {
            OrderedSelector.instance = new OrderedSelector(
                CustomSortService.getInstance()
            );
        }
        return OrderedSelector.instance;
    }


    /**
     * @param {CustomSortService} custom_sort_service
     */
    constructor(custom_sort_service) {
        this.custom_sort_service = custom_sort_service;
    }


    /**
     * @param {Array} list
     * @return {String}
     */
    process(list) {
        const ordered_to_publish_file_list = this.custom_sort_service
            .humanAlphaNumericalSort(list);

        const [
            first_folder,
        ] = ordered_to_publish_file_list;
        return first_folder;
    }

}

OrderedSelector.instance = null;

module.exports = {
    OrderedSelector,
};
