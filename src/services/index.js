'use strict';

const {
    TaskProcessorService,
} = require('./TaskProcessorService');
const {
    ConfigurationsLoaderService,
} = require('./ConfigurationsLoaderService');
const {
    TasksAutoManagerService,
} = require('./TasksAutoManagerService');
const {
    CommandLineParamsService,
} = require('./commons/CommandLineParamsService');

const commons = require('./commons');

module.exports = {
    TaskProcessorService,
    ConfigurationsLoaderService,
    TasksAutoManagerService,
    CommandLineParamsService,
    commons,
};
