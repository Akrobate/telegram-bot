'use strict';

const chokidar = require('chokidar');
const path = require('path');
const cron = require('node-cron');
const {
    logger,
} = require('../logger');
const {
    configuration,
} = require('../configuration');
const {
    ConfigurationsLoaderService,
} = require('./ConfigurationsLoaderService');
const {
    TaskProcessorService,
} = require('./TaskProcessorService');

class TasksAutoManagerService {

    /**
     * @static
     * @returns {TasksAutoManagerService}
     */
    static getInstance() {
        if (TasksAutoManagerService.instance === null) {
            TasksAutoManagerService.instance = new TasksAutoManagerService(
                ConfigurationsLoaderService.getInstance(),
                TaskProcessorService.getInstance()
            );
        }
        return TasksAutoManagerService.instance;
    }


    /**
     * @param {ConfigurationsLoaderService} configurations_loader_service
     * @param {TaskProcessorService} task_processor_service
     */
    constructor(
        configurations_loader_service,
        task_processor_service
    ) {
        this.configurations_loader_service = configurations_loader_service;
        this.task_processor_service = task_processor_service;
        this.data_folder = configuration.storage.folder;
        this.tasks_configuration_folder = path
            .join(this.data_folder, 'configurations/tasks/');
        this.tasks = {};

        this.dry_run = false;
    }


    /**
     * @param {Boolean} dry_run
     * @returns {Void}
     */
    setDryRun(dry_run) {
        this.dry_run = dry_run;
    }

    /**
     * @return {void}
     */
    async initAllTasks() {
        const tasks = await this.configurations_loader_service
            .loadAllTaksConfigurations(this.tasks_configuration_folder);
        Object.keys(tasks).forEach((task_key) => {
            this.setNewTask(task_key, tasks[task_key]);
        });
        this.watchTasksConfigurations();
    }


    /**
     * @return {void}
     */
    watchTasksConfigurations() {
        return chokidar
            .watch(this.tasks_configuration_folder, {
                ignoreInitial: true,
            })
            .on('add', async (_path) => {
                await this.upsertTask(_path);
            })
            .on('change', async (_path) => {
                await this.upsertTask(_path);
            })
            .on('unlink', async (_path) => {
                await this.deleteTask(_path);
            });
    }


    /**
     * @param {Object} _path
     * @return {void}
     */
    async upsertTask(_path) {
        let task = {};

        await new Promise((resolve) => setTimeout(() => resolve(), 1000));
        try {
            task = await this.configurations_loader_service
                .loadTaskConfiguration(_path);
        } catch (error) {
            logger.log(`${_path}: Not a configuration file`);
            logger.log(error);
            return;
        }

        try {
            const can_process = await this.checkTask(task);
            if (!can_process) {
                return;
            }
        } catch (error) {
            logger.log(`${_path}: Task : ${task.id} is unprocessable`);
            return;
        }

        const task_key = this.configurations_loader_service
            .getTaskKeyFromPath(_path);
        if (this.tasks[task_key] === undefined) {
            this.setNewTask(task_key, task);
        } else {
            this.unsetTask(task_key);
            this.setNewTask(task_key, task);
        }
    }


    /**
     * @param {Object} _path
     * @return {void}
     */
    deleteTask(_path) {
        const task_key = this.configurations_loader_service
            .getTaskKeyFromPath(_path);
        return this.unsetTask(task_key);
    }

    /**
     * @param {string} task_key
     * @param {Object} task
     * @return {void}
     */
    setNewTask(task_key, task) {
        this.tasks[task_key] = {
            task,
            cron_handler: this.initTaskCron(task),
        };
    }


    /**
     * @param {string} task_key
     * @param {Object} task
     * @return {void}
     */
    unsetTask(task_key) {
        if (this.tasks[task_key] === undefined) {
            return;
        }
        this.tasks[task_key].cron_handler.stop();
        this.tasks[task_key].cron_handler = null;
        this.tasks[task_key] = undefined;
        delete this.tasks[task_key];
    }


    /**
     * @param {Object} task
     * @param {Boolean} scheduled
     * @return {void}
     */
    initTaskCron(task) {
        const {
            crontab,
            active,
        } = task;
        return cron.schedule(
            crontab,
            () => {
                try {
                    this.processTask(task);
                } catch (error) {
                    logger.log(error);
                }
            },
            {
                scheduled: active === undefined ? false : active,
            }
        );
    }


    /**
     * @param {Object} task
     * @returns {Promise<Object>}
     */
    processTask(task) {
        logger.log('processing task', task.id, task.name, task.crontab);
        if (this.dry_run) {
            return {};
        }
        return this.task_processor_service.process(task);
    }


    /**
     * @param {Object} task
     * @returns {Promise<Object>}
     */
    checkTask(task) {
        logger.log('checking task', task.id, task.name, task.crontab);
        if (this.dry_run) {
            return true;
        }
        return this.task_processor_service.check(task);
    }
}

TasksAutoManagerService.instance = null;

module.exports = {
    TasksAutoManagerService,
};
