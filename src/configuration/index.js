'use strict';

const {
    configuration,
    Configuration,
} = require('./configuration');

module.exports = {
    configuration,
    Configuration,
};
