'use strict';

const {
    TasksAutoManagerService,
} = require('./services');

const tasks_auto_manager_service = TasksAutoManagerService.getInstance();

(async () => {
    await tasks_auto_manager_service.initAllTasks();
})();
