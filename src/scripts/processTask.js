/* istanbul ignore file */

'use strict';

const path = require('path');
const {
    TaskProcessorService,
    ConfigurationsLoaderService,
} = require('../services');
const {
    configuration,
} = require('../configuration');
const {
    logger,
} = require('../logger');
const task_filename = 'task_3.json';

const task_processor_service = TaskProcessorService.getInstance();
const configurations_loader_service = ConfigurationsLoaderService.getInstance();

(async () => {

    const task_object = await configurations_loader_service
        .loadTaskConfiguration(
            path.join(
                configuration.storage.folder,
                'configurations/tasks/',
                task_filename
            )
        );

    await task_processor_service
        .process(task_object);

    logger.log('Task published');
})();
