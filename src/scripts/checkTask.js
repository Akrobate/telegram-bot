/* istanbul ignore file */

'use strict';

const path = require('path');
const {
    TaskProcessorService,
    ConfigurationsLoaderService,
    commons,
} = require('../services');

const {
    CommandLineParamsService,
} = commons;

const {
    configuration,
} = require('../configuration');
const {
    logger,
} = require('../logger');

const {
    array_params,
} = (new CommandLineParamsService(logger)).setCommandLineParamsSchemaAndProcess({
    array_params: [
        {
            type: 'String',
            required: true,
            help: 'Configuration file name (example: task_3.json)',
        },
    ],
});

const [
    task_filename,
] = array_params;

const task_processor_service = TaskProcessorService.getInstance();
const configurations_loader_service = ConfigurationsLoaderService.getInstance();

(async () => {

    const task_object = await configurations_loader_service
        .loadTaskConfiguration(
            path.join(
                configuration.storage.folder,
                'configurations/tasks/',
                task_filename
            )
        );
    const result = await task_processor_service
        .check(task_object);

    logger.log(result);

})();
