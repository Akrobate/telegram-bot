'use strict';

const {
    TelegramRepository,
} = require('./TelegramRepository');
const {
    JsonFileRepository,
} = require('./JsonFileRepository');
const {
    FileRepository,
} = require('./FileRepository');

module.exports = {
    TelegramRepository,
    FileRepository,
    JsonFileRepository,
};
