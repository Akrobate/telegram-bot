'use strict';

const axios = require('axios');
const fs = require('fs');
const FormData = require('form-data');

class TelegramRepository {


    // eslint-disable-next-line require-jsdoc
    static get URL_HOST() {
        return 'https://api.telegram.org';
    }


    /**
     * @returns {TelegramRepository}
     */
    static getInstance() {
        if (TelegramRepository.instance === null) {
            TelegramRepository.instance = new TelegramRepository();
        }
        return TelegramRepository.instance;
    }


    /**
     * @param {string} api_key
     * @returns {Object}
     */
    async getMe(api_key) {
        const {
            data,
        } = await axios.get(
            `${this.getPreparedUrl(api_key)}/getMe`
        );
        return data;
    }


    /**
     * @param {*} api_key
     * @param {*} chat_id
     * @param {*} text
     * @returns {Object}
     */
    async sendMessage(api_key, chat_id, text) {
        const {
            data,
        } = await axios.get(
            `${this.getPreparedUrl(api_key)}/sendMessage`,
            {
                params: {
                    chat_id,
                    text,
                },
            }
        );
        return data;
    }


    /**
     * @param {*} api_key
     * @param {*} chat_id
     * @param {*} message_id
     * @returns {Object}
     */
    async deleteMessage(api_key, chat_id, message_id) {
        const {
            data,
        } = await axios.get(
            `${this.getPreparedUrl(api_key)}/deleteMessage`,
            {
                params: {
                    chat_id,
                    message_id,
                },
            }
        );
        return data;
    }


    /**
     * @param {string} api_key
     * @param {string} chat_id
     * @param {string} file_path
     * @param {string} caption
     * @param {string} parse_mode
     * @returns {Object}
     */
    async sendPhoto(api_key, chat_id, file_path, caption, parse_mode) {
        const form = new FormData();
        form.append('chat_id', chat_id);
        if (caption) {
            form.append('caption', caption);
        }
        if (parse_mode) {
            form.append('parse_mode', parse_mode);
        }
        form.append('photo', fs.createReadStream(file_path));
        const {
            data,
        } = await axios.post(
            `${this.getPreparedUrl(api_key)}/sendPhoto`,
            form
        );
        return data;
    }


    /**
     * @param {string} api_key
     * @returns {string}
     */
    getPreparedUrl(api_key) {
        return `${this.constructor.URL_HOST}/bot${api_key}`;
    }
}


TelegramRepository.instance = null;

module.exports = {
    TelegramRepository,
};
