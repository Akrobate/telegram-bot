'use strict';

const fs = require('fs');
const path = require('path');

class FileRepository {

    /**
     * @static
     * @returns {FileRepository}
     */
    static getInstance() {
        if (FileRepository.instance === null) {
            FileRepository.instance = new FileRepository();
        }
        return FileRepository.instance;
    }

    /**
     * @param {Object} directory
     * @return {Array}
     */
    async listFilesInDirectory(directory) {
        const file_list = await fs.promises.readdir(directory, {
            withFileTypes: true,
        });
        return file_list
            .filter((dirent) => dirent.isFile())
            .map((item) => item.name);
    }


    /**
     * @param {Array} folder_list
     * @param {Array} extention_list
     * @returns {String}
     */
    async listFilesInDirectoryWithExtention(folder_list, extention_list = []) {
        const file_list = await this
            .listFilesInDirectory(folder_list);

        return file_list.filter((file) => {
            const exploded = file.split('.');

            const extention = exploded.length > 1
                ? exploded[exploded.length - 1]
                : null;

            if (extention === null) {
                return (extention_list.length === 0);
            }

            return extention_list.map((item) => item.toLowerCase())
                .includes(extention.toLowerCase());
        });


    }


    /**
     * @param {Object} directory
     * @return {Array}
     */
    async listDirectoriesInDirectory(directory) {
        const file_list = await fs.promises.readdir(directory, {
            withFileTypes: true,
        });
        return file_list
            .filter((dirent) => dirent.isDirectory())
            .map((item) => item.name);
    }


    /**
     * @param {*} from_file
     * @param {*} to_file
     * @return {Object}
     */
    async moveFile(from_file, to_file) {
        const result = await fs.promises
            .rename(from_file, to_file);
        return result;
    }


    /**
     * @param {string} file
     * @param {string} content
     * @returns {void}
     */
    async appendFile(file, content) {
        const result = await fs.promises.appendFile(file, content);
        return result;
    }


    /**
     * @param {string} file_path
     * @returns {Boolean}
     */
    exists(file_path) {
        const exists = fs.existsSync(file_path);
        return exists;
    }


    /**
     * @param {string} filename
     * @returns {string}
     */
    async readFileUtf8(filename) {
        const string_data = await fs.promises.readFile(
            filename,
            'utf8'
        );
        return string_data;
    }


    /**
     * @param {String} filename
     * @param {String} extention
     * @returns {Boolean}
     */
    hasExtention(filename, extention) {
        const ext = path.extname(filename);
        if (ext) {
            const formated_ext = ext.replace('.', '').toLowerCase();
            return formated_ext === extention.toLowerCase();
        }
        return false;
    }
}

FileRepository.instance = null;

module.exports = {
    FileRepository,
};
