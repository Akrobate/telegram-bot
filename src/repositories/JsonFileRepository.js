'use strict';

const fs = require('fs').promises;

class JsonFileRepository {

    /* istanbul ignore next */
    /**
     * @static
     * @returns {JsonFileRepository}
     */
    static getInstance() {
        if (JsonFileRepository.instance === null) {
            JsonFileRepository.instance = new JsonFileRepository();
        }
        return JsonFileRepository.instance;
    }

    /**
     * @param {String} filename
     * @returns {Object}
     */
    async getData(filename) {
        const string_data = await fs.readFile(filename);
        return JSON.parse(string_data);
    }


    /* istanbul ignore next */
    /**
     * @param {Object} data
     * @param {String} filename
     * @return {Object}
     */
    async saveData(data, filename) {
        const string_data = JSON.stringify(data);
        const response = await fs.writeFile(
            filename,
            string_data
        );
        return response;
    }


    /* istanbul ignore next */
    /**
     * @param {String} filename
     * @return {Object}
     */
    async removeData(filename) {
        const response = await fs.unlink(filename);
        return response;
    }
}

JsonFileRepository.instance = null;

module.exports = {
    JsonFileRepository,
};
