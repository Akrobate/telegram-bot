# telegram-bot

## Requirements

* nodejs >16


## Docker usage

docker-compose build --no-cache
docker-compose up -d --force-recreate 


## Todo

* Split entry data types
* Split behaviours publications
* Combinations purpose